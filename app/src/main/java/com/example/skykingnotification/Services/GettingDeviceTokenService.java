package com.example.skykingnotification.Services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class GettingDeviceTokenService extends FirebaseInstanceIdService {
    private static final String TAG = "GettingDeviceToken";
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String DeviceToken = FirebaseInstanceId.getInstance().getToken();
        Log.i(TAG,"Device Token : " + DeviceToken );
    }
}
