package com.example.skykingnotification.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.skykingnotification.R;

public class BasicUtils {
    private static final String TAG = "BasicUtils";
    Context context;
    Dialog dialog;
    public Toast toast;

    public BasicUtils(Context context) {
        this.context = context;
        this.toast = new Toast(context);
    }

    public void showProgress (boolean isShow){
        if (isShow) {
            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable
                    (new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.setContentView(R.layout.dialog_progress);
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            dialog.show();
        } else {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
        }
    }

    public void hideKeyboard(CoordinatorLayout cL) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(cL.getWindowToken(), 0);
    }

    public void setToast(String sText) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View toastRoot = inflater.inflate(R.layout.toast, null);

        toast.setView(toastRoot);

        // set a message
        TextView text = (TextView) toastRoot.findViewById(R.id.tvToast);
        text.setText(sText);

        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM,
                0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }
}
