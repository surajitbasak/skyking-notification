package com.example.skykingnotification.utils;

public class Constants {

    public static final boolean IS_LOCAL                                                            = false;

    public static final String LOCAL                                                                = "http://202.142.102.195:8899/api/";
    public static final String PRODUCTION                                                           = "https://live.skyking.co/api/";

    public static final String URL                                                                  = IS_LOCAL ? LOCAL : PRODUCTION;
    public static final String MOBILE                                                               = URL + "mobileapi/";

    public static final String API_GET_NOTIFICATION                                                 = MOBILE + "PushNotification";
    public static final String API_SEND_NOTIFICATION                                                = "https://fcm.googleapis.com/fcm/send";

    public static final String CONTENT_TYPE                                                         = "application/json";

    public static final String SERVER_KEY                                                           = "AAAAck_pqyw:APA91bHvWohfF1_dPzwnJuJ5O3" +
                                                                                                      "LvuEmas7S0jzXQoGnKmiKo45tqAFv39y45zzkp" +
                                                                                                      "Cuv07GqkBgUKPVAXYSkpi3kqncc09Byu5Erdvf" +
                                                                                                      "1Wpuk17lCElTkh8kLZ0iA9JBsEw7uu76kSTVs_";

    public static final String TEST_SERVER_KEY                                                      = "AAAAmQEEXIM:APA91bGFrrOA2JBwyknFteDXtD" +
                                                                                                      "1jiYHbGLTohNPf9RcaHkK4KpzrhwuAcZqMRWFI" +
                                                                                                      "ZqUjI8K3JLncjAot7uYh5VfVtPei5Lo0G2hPF5" +
                                                                                                      "ibaJUDi0WcnJAj5wp9lOwBa77P1lnefbIMHXDA";

    public static final boolean requestPassword                                                     = false;
}
//=====