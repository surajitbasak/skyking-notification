package com.example.skykingnotification.Services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.example.skykingnotification.Activitys.MainActivity;
import com.example.skykingnotification.R;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.RemoteMessage;

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {
    private static final String TAG = "FirebaseMessaging";
    public static final String CHANNEL_ID = "2020";
    public static  int NOTIFICATION_ID = 0;
    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.i(TAG,"TOKEN UPDATE : " + s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        String title = remoteMessage.getData().get("title");
        String message = remoteMessage.getData().get("message");
        String titleImageURL = remoteMessage.getData().get("title_url");
        String bannerImageURL = remoteMessage.getData().get("banner_url");
        Intent i = new Intent(this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,i,PendingIntent.FLAG_ONE_SHOT);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O){
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,"Tracking Notifications",NotificationManager.IMPORTANCE_HIGH);
            channel.setDescription("Notifications of Special Occasion");
            manager.createNotificationChannel(channel);
        }
        NotificationCompat.Builder notificationCompat = new NotificationCompat.Builder(this,CHANNEL_ID);
        notificationCompat.setContentTitle(title);
        notificationCompat.setContentText(message);
        notificationCompat.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
        notificationCompat.setAutoCancel(true);
        notificationCompat.setColor(getResources().getColor(R.color.colorPurple));
        notificationCompat.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        notificationCompat.setPriority(NotificationCompat.PRIORITY_MAX);
        notificationCompat.setSmallIcon(R.mipmap.small_logo);
        notificationCompat.setContentIntent(pendingIntent);
        volleyGetTitleImage(titleImageURL,bannerImageURL,notificationCompat,manager);
    }


    private void volleyGetTitleImage(final String titleImageUrl ,
                                     final String bannerImageUrl,
                                     final NotificationCompat.Builder notiCompat,
                                     final NotificationManager manager ) {
        Log.i(TAG,"volleyGetTitleImage URL = " + titleImageUrl);

        final ImageRequest iRequest = new ImageRequest(titleImageUrl, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                Log.i(TAG,"volleyGetTitleImage Response = " + response.toString());
                notiCompat.setLargeIcon(response);
                if (bannerImageUrl.trim().length() != 0){
                    volleyGetBannerImage(bannerImageUrl,notiCompat,manager);
                }
                else {
                    if (NOTIFICATION_ID > 200){
                        NOTIFICATION_ID = 0;
                    }
                    manager.notify(NOTIFICATION_ID ++ , notiCompat.build());
                }
            }
        }, 0, 0, Bitmap.Config.RGB_565, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG,"volleyGetTitleImage Error = " + error.toString());
                Log.e(TAG,"volleyGetTitleImage Error Message = " + error.getMessage());
            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(iRequest);
        iRequest.setRetryPolicy(
                new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 3,
                        0,  //Since Multiple bids are being placed if retry is hit as response is delayed but DB captures the bid
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private void volleyGetBannerImage(final String bannerImageUrl,
                                      final NotificationCompat.Builder notiCompat,
                                      final NotificationManager manager){
        Log.i(TAG,"volleyGetBannerImage URL = " + bannerImageUrl);

        final ImageRequest iRequest = new ImageRequest(bannerImageUrl, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                Log.i(TAG,"volleyGetBannerImage Response = " + response.toString());
                notiCompat.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(response));
                if (NOTIFICATION_ID > 200){
                    NOTIFICATION_ID = 0;
                }
                manager.notify(NOTIFICATION_ID ++ , notiCompat.build());
            }
        }, 0, 0, Bitmap.Config.RGB_565, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG,"volleyGetBannerImage Error = " + error.toString());
                Log.i(TAG,"volleyGetBannerImage Error Message = " + error.getMessage());
            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(iRequest);
        iRequest.setRetryPolicy(
                new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 3,
                        0,  //Since Multiple bids are being placed if retry is hit as response is delayed but DB captures the bid
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }
}
