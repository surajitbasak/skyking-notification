package com.example.skykingnotification.utils;

import android.content.Context;

import com.example.skykingnotification.items.NotificationItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ApiProcessing {

    public static class NotificationManager {

        public static ArrayList<NotificationItem> parseResponse(JSONArray response, Context context) throws JSONException {
            ArrayList<NotificationItem> notiItem = new ArrayList<>();
            for (int i = 0; i < response.length(); i++) {
                JSONObject object = response.getJSONObject(i);
                NotificationItem nItem = new NotificationItem();
                nItem.setTitle(object.getString("Title"));
                nItem.setMessage(object.getString("Message"));
                nItem.setTitleImageUrl(object.getString("TitleImageURL"));
                nItem.setBannerImageUrl(object.getString("BannerImageURL"));
                nItem.setStatus(object.getString("Status"));

                notiItem.add(nItem);
            }
            return notiItem;
        }

        public static JSONObject constructObject(String title, String message, String titleImageURL, String bannerImageURL, int type, String token) {
            JSONObject object = new JSONObject();
            JSONObject subObject = new JSONObject();
            try {
                subObject.put("title", title);
                subObject.put("body", message);
                subObject.put("key_1", titleImageURL);
                subObject.put("key_2", bannerImageURL);

                object.put("data", subObject);
                if (type == 1)
                    object.put("to", "/topics/" + "tracking");// HardCoded Target User
                else if (type == 2)
                    object.put("to", "/topics/" + "delivery");// HardCoded Target User
                else
                    object.put("to", token);// Device Specific Target User
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return object;
        }
    }
}
