package com.example.skykingnotification.items;

public class NotificationItem {

    public String title;
    public String message;
    public String titleImageUrl;
    public String bannerImageUrl;
    public String status;
    public boolean selected = false;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTitleImageUrl() {
        return titleImageUrl;
    }

    public void setTitleImageUrl(String titleImageUrl) {
        this.titleImageUrl = titleImageUrl;
    }

    public String getBannerImageUrl() {
        return bannerImageUrl;
    }

    public void setBannerImageUrl(String bannerImageUrl) {
        this.bannerImageUrl = bannerImageUrl;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public String toString() {
        return "NotificationItem{" +
                "title='" + title + '\'' +
                ", message='" + message + '\'' +
                ", titleImageUrl='" + titleImageUrl + '\'' +
                ", bannerImageUrl='" + bannerImageUrl + '\'' +
                ", status='" + status + '\'' +
                ", selected=" + selected +
                '}';
    }
}
