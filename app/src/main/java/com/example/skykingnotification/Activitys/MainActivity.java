package com.example.skykingnotification.Activitys;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.skykingnotification.Adapters.NotificationAdapter;
import com.example.skykingnotification.R;
import com.example.skykingnotification.items.NotificationItem;
import com.example.skykingnotification.utils.ApiProcessing;
import com.example.skykingnotification.utils.BasicUtils;
import com.example.skykingnotification.utils.Constants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    public Context context;
    BasicUtils basicUtils;
    CoordinatorLayout cL;
    CardView cvSelectAll;
    CardView cvSendTracking;
    CardView cvSendDelivery;
    TextView selectText;
    TextView tvDate;
    TextView tvTime;
    TextView tvCount;

    RecyclerView recyclerView;
    LinearLayoutManager manager;
    SwipeRefreshLayout swipeRefreshLayout;

    LinearLayout no_Notification;
    FrameLayout notification_found;
    ToggleButton toggleButton;

    ArrayList<NotificationItem> arraylist;
    ArrayList<NotificationItem> filterList;
    NotificationAdapter adapter;

    AutoCompleteTextView search;

    public final String DATE_FORMAT = "dd MMM yyyy";
    public final String TIME_FORMAT = "HH:mm:ss";

    boolean isSelectAll = true;

    public String DEVICE_TOKEN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorYellow));
        }

        FirebaseMessaging.getInstance().subscribeToTopic("thisapp")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task != null && task.isSuccessful())
                            Log.i(TAG, "SUBSCRIPTION STATUS = " + "SUCCESS");
                        else
                            Log.i(TAG, "SUBSCRIPTION STATUS = " + "FAILED");
                    }
                });

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (task.isSuccessful()) {
                            DEVICE_TOKEN = task.getResult().getToken();
                            Log.i(TAG, "Device Token = " + DEVICE_TOKEN);
                        } else {
                            Log.i(TAG, "Device Token = " + "NOT GENERATED");
                        }
                    }
                });
        init();
        dateTimeSetter();
        volleyGetNotification(swipeRefreshLayout, cvSendTracking, cvSendDelivery);
    }

    private void dateTimeSetter() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
        SimpleDateFormat timeFormat = new SimpleDateFormat(TIME_FORMAT, Locale.ENGLISH);

        String date = dateFormat.format(calendar.getTime());
        String time = timeFormat.format(calendar.getTime());

        tvDate.setText(date);
        tvTime.setText(time);

        new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                dateTimeSetter();
            }

            @Override
            public void onFinish() {
            }
        }.start();
    }

    private void init() {

        cL = findViewById(R.id.cL);
        basicUtils = new BasicUtils(context);
        recyclerView = findViewById(R.id.recyclerView);
        no_Notification = findViewById(R.id.no_notification);
        notification_found = findViewById(R.id.fl_notificationFound);
        swipeRefreshLayout = findViewById(R.id.swipe);
        manager = new LinearLayoutManager(context);

        cvSelectAll = findViewById(R.id.cvSelectAll);
        cvSendTracking = findViewById(R.id.cvSendTracking);
        cvSendDelivery = findViewById(R.id.cvSendDelivery);
        search = findViewById(R.id.search_notification);

        selectText = findViewById(R.id.selectText);
        tvDate = findViewById(R.id.tvDate);
        tvTime = findViewById(R.id.tvTime);
        tvCount = findViewById(R.id.tvCount);

        toggleButton = findViewById(R.id.production_toggle);

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_password);

        final EditText password = dialog.findViewById(R.id.dialog_et_password);
        CardView done = dialog.findViewById(R.id.dialog_done);
        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (password.getText().toString().trim().equals("1243")) {
                    dialog.dismiss();
                } else if (password.getText().toString().trim().length() == 0) {
                    password.setError("Enter Password");
                    password.requestFocus();
                } else if (!password.getText().toString().trim().equals("1243")) {
                    password.setError("Wrong Password!");
                    password.requestFocus();
                } else {
                    dialog.dismiss();
                }
                return false;
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (password.getText().toString().trim().equals("1243")) {
                    dialog.dismiss();
                } else if (password.getText().toString().trim().length() == 0) {
                    password.setError("Enter Password");
                    password.requestFocus();
                } else if (!password.getText().toString().trim().equals("1243")) {
                    password.setError("Wrong Password!");
                    password.requestFocus();
                } else {
                    dialog.dismiss();
                }
            }
        });
        if (Constants.requestPassword)
            dialog.show();
        clickManager();
    }

    private void clickManager() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                volleyGetNotification(swipeRefreshLayout, cvSendTracking, cvSendDelivery);
            }
        });

        cvSelectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSelectAll) {
                    adapter.select_remove_all(1);
                    selectText.setText("Remove All");
                    isSelectAll = false;
                } else {
                    adapter.select_remove_all(2);
                    selectText.setText("Select All");
                    isSelectAll = true;
                }
            }
        });

        cvSendTracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (toggleButton.isChecked()) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogStyle);
                    builder.setCancelable(true);
                    builder.setIcon(R.mipmap.send);
                    if (adapter.getSendCount().equals("1"))
                        builder.setMessage("Send " + adapter.getSendCount() + " Notification to Tracking App?");
                    else
                        builder.setMessage("Send " + adapter.getSendCount() + " Notifications to Tracking App?");

                    builder.setTitle("Confirm?");

                    builder.setPositiveButton("SEND", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            adapter.sendTracking();
                        }
                    });
                    builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    if (adapter.getSendCount().equals("0"))
                        basicUtils.setToast("No Notification is Selected!");
                    else
                        builder.show();
                } else
                    adapter.sendToThis();
            }
        });
        cvSendDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (toggleButton.isChecked()) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogStyle);
                    builder.setCancelable(true);
                    builder.setIcon(R.mipmap.send);
                    if (adapter.getSendCount().equals("1"))
                        builder.setMessage("Send " + adapter.getSendCount() + " Notification to Delivery App?");
                    else
                        builder.setMessage("Send " + adapter.getSendCount() + " Notifications to Delivery App?");

                    builder.setTitle("Confirm?");

                    builder.setPositiveButton("SEND", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            adapter.sendDelivery();
                        }
                    });
                    builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    if (adapter.getSendCount().equals("0"))
                        basicUtils.setToast("No Notification is Selected!");
                    else
                        builder.show();
                } else
                    adapter.sendToThis();
            }
        });

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (arraylist.size() > 0 && arraylist != null) {
                    filterText(s.toString());
                }
            }
        });

        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                basicUtils.hideKeyboard(cL);
                return false;
            }
        });
    }

    private void filterText(String text) {
        filterList = new ArrayList<>();
        String value = text.toLowerCase().trim();
        if (value.length() > 0 && arraylist != null) {
            for (NotificationItem nItem : arraylist) {
                if (nItem.getTitle().toLowerCase().trim().contains(value) || nItem.getMessage().toLowerCase().trim().contains(value)) {
                    filterList.add(nItem);
                }
            }
        } else {
            filterList.addAll(arraylist);
        }

        if (filterList.size() == 0) {
            notification_found.setBackground(getResources().getDrawable(R.drawable.rectangle_red));
            no_Notification.setVisibility(View.VISIBLE);
        } else {
            notification_found.setBackground(getResources().getDrawable(R.drawable.rectangle_green));
            no_Notification.setVisibility(View.GONE);

        }
        adapter = new NotificationAdapter(filterList, context, cL, DEVICE_TOKEN);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        if (value.length() == 0) {
            notification_found.setBackground(getResources().getDrawable(R.drawable.rectangle_grey));
        }
        tvCount.setText(String.valueOf(filterList.size()));
    }

    public void volleyGetNotification(final SwipeRefreshLayout swipe, final CardView cvSendTracking, final CardView cvSendDelivery) {
        basicUtils.showProgress(true);
        final String url = Constants.API_GET_NOTIFICATION;
        Log.i(TAG, "volleyGetNotification URL = " + url);
        cvSendTracking.setEnabled(false);
        cvSendDelivery.setEnabled(false);
        final JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        if (search.getText().toString().trim().length() != 0)
                            search.setText("");
                        basicUtils.showProgress(false);
                        cvSendTracking.setEnabled(true);
                        cvSendDelivery.setEnabled(true);
                        selectText.setText("Select All");
                        isSelectAll = true;
                        swipe.setRefreshing(false);
                        Log.i(TAG, "volleyGetNotification Response = " + response.toString());
                        if (response.length() == 0)
                            no_Notification.setVisibility(View.VISIBLE);
                        else
                            no_Notification.setVisibility(View.GONE);
                        try {
                            arraylist = ApiProcessing.NotificationManager.parseResponse(response, context);
                            recyclerView.setLayoutManager(manager);
                            adapter = new NotificationAdapter(arraylist, context, cL, DEVICE_TOKEN);
                            recyclerView.setAdapter(adapter);
                            tvCount.setText(String.valueOf(arraylist.size()));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(TAG, "volleyGetNotification Exception = " + e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                no_Notification.setVisibility(View.VISIBLE);
                basicUtils.showProgress(false);
                swipe.setRefreshing(false);
                basicUtils.setToast("Timed Out!");
                Log.e(TAG, "volleyGetNotification Error = " + error.toString());
                Log.e(TAG, "volleyGetNotification Error Message = " + error.getMessage());
            }
        });
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);
        request.setRetryPolicy(
                new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 3,
                        0,  //Since Multiple bids are being placed if retry is hit as response is delayed but DB captures the bid
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

}
