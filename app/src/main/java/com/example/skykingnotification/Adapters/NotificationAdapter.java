package com.example.skykingnotification.Adapters;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.provider.SyncStateContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.skykingnotification.R;
import com.example.skykingnotification.items.NotificationItem;
import com.example.skykingnotification.utils.ApiProcessing;
import com.example.skykingnotification.utils.BasicUtils;
import com.example.skykingnotification.utils.Constants;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
    private static final String TAG = "NotificationAdapter";
    ArrayList<NotificationItem> notificationList;
    Context context;
    CoordinatorLayout cL;
    BasicUtils basicUtils;
    String TOKEN;

    public NotificationAdapter(ArrayList<NotificationItem> notificationList, Context context
            , CoordinatorLayout coordinatorLayout, String token) {
        this.notificationList = notificationList;
        this.context = context;
        this.cL = coordinatorLayout;
        this.TOKEN = token;
        basicUtils = new BasicUtils(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_notification, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        if (notificationList.get(position).getStatus().equals("1")) {
            holder.title.setText(notificationList.get(position).getTitle());
            holder.message.setText(notificationList.get(position).getMessage());
            holder.titleImageUrl.setText(notificationList.get(position).getTitleImageUrl());
            holder.bannerImageUrl.setText(notificationList.get(position).getBannerImageUrl());

            holder.preview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Dialog dialog = new Dialog(context, R.style.DialogZoomAnim);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.setContentView(R.layout.dialog_notification_preview);
                    dialog.setCancelable(true);
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                    TextView title = dialog.findViewById(R.id.preview_title);
                    TextView message = dialog.findViewById(R.id.preview_message);
                    ImageView title_image = dialog.findViewById(R.id.title_pic);
                    ImageView banner_image = dialog.findViewById(R.id.banner_pic);

                    if (notificationList.get(position).getTitle().length() > 35)
                        title.setText(notificationList.get(position).getTitle().substring(0, 35) + " ...");
                    else
                        title.setText(notificationList.get(position).getTitle());

                    if (notificationList.get(position).getMessage().length() > 35 && notificationList.get(position).getBannerImageUrl().length() != 0)
                        message.setText(notificationList.get(position).getMessage().substring(0, 35) + " ...");
                    else
                        message.setText(notificationList.get(position).getMessage());

                    if (notificationList.get(position).getTitleImageUrl().length() == 0)
                        title_image.setVisibility(View.GONE);
                    else
                        Picasso.get()
                                .load(notificationList.get(position).getTitleImageUrl())
                                .placeholder(R.mipmap.loading)
                                .error(R.mipmap.error_image_loading)
                                .into(title_image);
                    if (notificationList.get(position).getBannerImageUrl().length() == 0)
                        banner_image.setVisibility(View.GONE);
                    else
                        Picasso.get()
                                .load(notificationList.get(position).getBannerImageUrl())
                                .placeholder(R.mipmap.loading)
                                .error(R.mipmap.error_image_loading)
                                .into(banner_image);

                    dialog.show();
                }
            });

            holder.checkBox.setChecked(notificationList.get(position).isSelected());

            holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    notificationList.get(position).setSelected(isChecked);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    public void sendTracking() {
        try {
            volleySendNotification(1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void sendDelivery() {
        try {
            volleySendNotification(2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void sendToThis() {
        try {
            volleySendNotification(3);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getSendCount(){
        int count = 0;
        for (NotificationItem nItem : notificationList){
            if (nItem.isSelected())
                count ++ ;
        }
        return String.valueOf(count);
    }

    public void select_remove_all(int value) {
        if (value == 1) {
            for (NotificationItem nItem : notificationList) {
                nItem.setSelected(true);
            }
            notifyDataSetChanged();
        } else {
            for (NotificationItem nItem : notificationList) {
                nItem.setSelected(false);
            }
            notifyDataSetChanged();
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView message;
        TextView titleImageUrl;
        TextView bannerImageUrl;
        CheckBox checkBox;
        CardView preview;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.row_title);
            message = itemView.findViewById(R.id.row_message);
            titleImageUrl = itemView.findViewById(R.id.row_title_image_url);
            bannerImageUrl = itemView.findViewById(R.id.row_banner_image_url);
            checkBox = itemView.findViewById(R.id.row_checkBox);
            preview = itemView.findViewById(R.id.notification_preview);
        }
    }

    public void volleySendNotification(int type) throws JSONException {
        int count = 0;
        final String url = Constants.API_SEND_NOTIFICATION;
        Log.i(TAG, "volleySendNotification URL = " + url);

        for (NotificationItem nItem : notificationList) {

            if (nItem.isSelected()) {

                JSONObject object = ApiProcessing.NotificationManager.constructObject(nItem.getTitle(),
                        nItem.getMessage(), nItem.getTitleImageUrl(), nItem.getBannerImageUrl(), type,TOKEN);
                Log.i(TAG, "volleySendNotification SEND = " + object.toString());

                final JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.POST,
                        url,
                        object,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                basicUtils.showProgress(false);
                                Log.i(TAG, "volleySendNotification Response = " + response.toString());
                                try {
                                    String message_id = response.getString("message_id");
                                    final Snackbar snackbar = Snackbar.make(cL, "Message Id : " + message_id, Snackbar.LENGTH_LONG);
                                    snackbar.show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.e(TAG, "volleySendNotification Exception = " + e.getMessage());
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        basicUtils.showProgress(false);
                        basicUtils.setToast("Timed Out!");
                        Log.e(TAG, "volleySendNotification Error = " + error.toString());
                        Log.e(TAG, "volleySendNotification Error Message = " + error.getMessage());
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> map = new HashMap<>();
                        map.put("Content-Type", Constants.CONTENT_TYPE);
                        map.put("Authorization", "key=" + Constants.SERVER_KEY);
                        return map;
                    }
                };
                RequestQueue queue = Volley.newRequestQueue(context);
                queue.add(objectRequest);
                objectRequest.setRetryPolicy(
                        new DefaultRetryPolicy(
                                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 3,
                                0,  //Since Multiple bids are being placed if retry is hit as response is delayed but DB captures the bid
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            }
        }
        for (int i = 0; i < notificationList.size(); i++) {
            if (!notificationList.get(i).isSelected())
                count++;
        }
        if (count == notificationList.size())
            basicUtils.setToast("No Notification is Selected!");
        else
            basicUtils.showProgress(true);
    }

}
